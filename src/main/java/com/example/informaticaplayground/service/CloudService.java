package com.example.informaticaplayground.service;

import com.example.informaticaplayground.model.*;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CloudService {

    private static final String PROCESSING_ERROR_MESSAGE = "Error while processing the file provided";

    public BatchFileContent processFile(InputStream file) {
        CSVReader reader = new CSVReader(new InputStreamReader(file));
        BatchFileContent response;
        try {
            String[] headers = reader.readNext();
            List<String> headersList = Objects.nonNull(headers) ? Arrays.asList(headers) : Collections.emptyList();
            response = getBatchFileContent(headersList);
        } catch (IOException | CsvValidationException ex) {
            throw new IllegalArgumentException(PROCESSING_ERROR_MESSAGE, ex);
        }

        if (Objects.nonNull(response)) {
            for (String[] line : reader) {
                BatchFileRecord batchFileRecord = new BatchFileRecord();
                for (int i = 0; i < response.getHeaders().size(); i++) {
                    BatchFileContent.BatchFileHeader header = response.getHeaders().get(i);
                    batchFileRecord.addColumn(processValue(response.getEntity(), header, line[i]));
                }
                response.addRecord(batchFileRecord);
            }
        }


        return response;
    }

    private static final Set<BatchFileContent.BatchFileHeader> CONSUMER_ACCESS_HEADER = new HashSet<>(Arrays.asList(
            new BatchFileContent.BatchFileHeader("Status", BatchFileContent.BatchFileHeaderType.STATUS),
            new BatchFileContent.BatchFileHeader("Data Collection",BatchFileContent.BatchFileHeaderType.DATA_COLLECTION),
            new BatchFileContent.BatchFileHeader("Delivery Target",BatchFileContent.BatchFileHeaderType.DELIVERY_TARGET),
            new BatchFileContent.BatchFileHeader("Data Consumer",BatchFileContent.BatchFileHeaderType.USER_CONSUMER),
            new BatchFileContent.BatchFileHeader("Date Access Granted",BatchFileContent.BatchFileHeaderType.DATE_TIME),
            new BatchFileContent.BatchFileHeader("Usage Context",BatchFileContent.BatchFileHeaderType.USAGE_CONTEXT)
    ));

    private BatchFileContent getBatchFileContent(List<String> headersList) {
        BatchFileContent response;
        List<BatchFileContent.BatchFileHeader> mappedHeaders = headersList.stream().map(header ->
                    CONSUMER_ACCESS_HEADER.stream().filter(batchFileHeader -> {
                        if (StringUtils.isNotBlank(header)) {
                            return batchFileHeader.getName().equalsIgnoreCase(header.trim());
                        } else {
                            return false;
                        }
                    }).findFirst().orElse(null)
                )
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        if (headersList.size() == mappedHeaders.size()) {
            response = new BatchFileContent()
                    .withEntity(BatchFileContent.EntityType.CONSUMER_ACCESS)
                    .withHeaders(mappedHeaders);
            return response;
        } else {
            throw new IllegalArgumentException("Entity could not be inferred from headers");
        }
    }

    private BatchFileObject processValue(BatchFileContent.EntityType entityType, BatchFileContent.BatchFileHeader header, String value) {
        String id = null;
        String text = value;

        if (!StringUtils.isBlank(value)) {
            switch (header.getType()) {
                case USER_CONSUMER:
                    APIUser user = DataService.findConsumerUserByEmail(value);
                    if (Objects.nonNull(user)) {
                        id = user.getId();
                        text = user.getName();
                    }
                    break;
                case STATUS:
                    if (BatchFileContent.EntityType.CONSUMER_ACCESS.equals(entityType)) {
                        if ("AVAILABLE".equalsIgnoreCase(value)) {
                            id = "AVAILABLE";
                        } else if ("WITHDRAWN".equalsIgnoreCase(value)) {
                            id = "WITHDRAWN";
                        } else if ("PENDING_WITHDRAW".equalsIgnoreCase(value) || "PENDING WITHDRAW".equalsIgnoreCase(value)) {
                            id = "PENDING_WITHDRAW";
                        }
                    }
                    break;
                case DATA_COLLECTION:
                    List<String> ids = DataService.getDataCollectionIdByName(value);
                    if (Objects.nonNull(ids) && Objects.equals(ids.size(), 1)) {
                        id = ids.get(0);
                    }
                    break;
                case DELIVERY_TARGET:
                    ids = DataService.getDeliveryTargetIdByName(value);
                    if (Objects.nonNull(ids) && Objects.equals(ids.size(), 1)) {
                        id = ids.get(0);
                    }
                    break;
                case DATE_TIME:
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
                    SimpleDateFormat targetDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    try {
                        Date date = simpleDateFormat.parse(value);
                        id = targetDateFormat.format(date);
                    } catch (ParseException ex) {
//                        addBatchFileError(batchFileErrors, ErrorCode.INVALID_FORMAT.getErrorCodeMessage(), ErrorCode.INVALID_FORMAT.name(), valueIndex, text);
                    }
                    break;
                case USAGE_CONTEXT:
//                    FetchedObjects<String> usageContextIds = usageContextRepository.getUsageContextIdsByName(value);
//                    if (Objects.nonNull(usageContextIds) && Objects.equals(usageContextIds.getTotalCount(), 1)) {
//                        id = usageContextIds.getObjects().get(0);
//                    } else if (Objects.nonNull(usageContextIds) && usageContextIds.getTotalCount() > 1) {
//                        addBatchFileError(batchFileErrors, ErrorCode.MULTIPLE_OPTIONS_FOUND.getErrorCodeMessage(), ErrorCode.MULTIPLE_OPTIONS_FOUND.name(), valueIndex, text);
//                    } else {
//                        addBatchFileError(batchFileErrors, ErrorCode.ELEMENT_NOT_FOUND.getErrorCodeMessage(), ErrorCode.ELEMENT_NOT_FOUND.name(), valueIndex, text);
//                    }
                    break;
            }
        }
        // usage context fix
        else if(BatchFileContent.BatchFileHeaderType.USAGE_CONTEXT.equals(header.getType())) {
            id = "usage-context-id";
        }

        return new BatchFileObject().addValue(new BatchFileObjectValue().withId(id).withText(text));
    }
}
