package com.example.informaticaplayground.service;

import com.example.informaticaplayground.model.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OnPremService {

    public ApiConsumerAccessListResponse fetchDataCollectionConsumerAccess(ApiConsumerAccessFilter filters) {
        List<ApiConsumerAccessResponse> response = DataService.onPremData.stream()
                .sorted((a,b) -> {
                    int comparisonResult = 0;
                    if(SortField.CREATION_TIMESTAMP.equals(filters.getSortField())) {
                        comparisonResult = a.getCreationTimestamp().compareTo(b.getCreationTimestamp());
                    } else if(SortField.UPDATED_TIMESTAMP.equals(filters.getSortField())) {
                        comparisonResult = a.getUpdatedTimestamp().compareTo(b.getUpdatedTimestamp());
                    } else if(SortField.STATUS.equals(filters.getSortField())) {
                        comparisonResult = a.getStatus().compareTo(b.getStatus());
                    }
                    return SortOrder.ASC.equals(filters.getSortOrder()) ? comparisonResult : -comparisonResult;
                })
                .skip(filters.getOffset())
                .limit(filters.getLimit())
                .collect(Collectors.toList());

        return new ApiConsumerAccessListResponse(
                response,
                Long.parseLong(String.valueOf(DataService.onPremData.size()))
        );
    }
}
