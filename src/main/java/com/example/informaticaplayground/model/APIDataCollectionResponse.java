package com.example.informaticaplayground.model;

import io.swagger.v3.oas.annotations.media.Schema;


public class APIDataCollectionResponse {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY, example = "01234567-89ab-cdef-0123-4567890abcde")
    private String id;

    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private String custRefId;

    private String name;

    private String description;

    public String getId() {
        return id;
    }

    public APIDataCollectionResponse withId(String id) {
        this.id = id;
        return this;
    }

    public String getCustRefId() {
        return custRefId;
    }

    public APIDataCollectionResponse withCustRefId(String custRefId) {
        this.custRefId = custRefId;
        return this;
    }

    public String getName() {
        return name;
    }

    public APIDataCollectionResponse withName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public APIDataCollectionResponse withDescription(String description) {
        this.description = description;
        return this;
    }
}
