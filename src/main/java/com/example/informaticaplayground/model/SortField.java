package com.example.informaticaplayground.model;

import com.fasterxml.jackson.annotation.JsonValue;

public enum SortField {
    CREATION_TIMESTAMP("creationTimestamp"),
    UPDATED_TIMESTAMP("updatedTimestamp"),
    STATUS("status");

    private final String value;

    SortField(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
