package com.example.informaticaplayground.model;

import java.util.ArrayList;
import java.util.List;

public class BatchFileRecord {
    private List<BatchFileObject> columns;

    public List<BatchFileObject> getColumns() {
        return columns;
    }

    public BatchFileRecord addColumn(BatchFileObject column) {
        if (this.columns == null) {
            this.columns = new ArrayList<>();
        }

        this.columns.add(column);
        return this;
    }
}
