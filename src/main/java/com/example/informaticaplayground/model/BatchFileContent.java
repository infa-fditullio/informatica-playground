package com.example.informaticaplayground.model;

import java.util.ArrayList;
import java.util.List;

public class BatchFileContent {
    private EntityType entity;
    private List<BatchFileHeader> headers;
    private List<BatchFileRecord> records;

    public EntityType getEntity() {
        return entity;
    }

    public BatchFileContent withEntity(EntityType entity) {
        this.entity = entity;
        return this;
    }

    public List<BatchFileHeader> getHeaders() {
        return headers;
    }

    public BatchFileContent withHeaders(List<BatchFileHeader> headers) {
        this.headers = headers;
        return this;
    }

    public List<BatchFileRecord> getRecords() {
        return records;
    }

    public void addRecord(BatchFileRecord fileRecord) {
        if (this.records == null) {
            this.records = new ArrayList<>();
        }

        this.records.add(fileRecord);
    }

    public enum EntityType {
        CONSUMER_ACCESS
    }

    public static class BatchFileHeader {
        private final String name;
        private final BatchFileHeaderType type;

        public BatchFileHeader(String name, BatchFileHeaderType type) {
            this.name = name;
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public BatchFileHeaderType getType() {
            return type;
        }
    }

    public enum BatchFileHeaderType {
        DATA_COLLECTION,
        DELIVERY_TARGET,
        USER_CONSUMER,
        DATE_TIME,
        STATUS,
        USAGE_CONTEXT;
    }
}
