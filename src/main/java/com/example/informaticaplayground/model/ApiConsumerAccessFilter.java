package com.example.informaticaplayground.model;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class ApiConsumerAccessFilter {

    @Min(value = 0)
    @NotNull
    private Integer offset = 0;

    @Min(value = 0)
    @NotNull
    @Schema(example = "20")
    private Integer limit = 20;

    @NotNull
    @Schema(example = "creationTimestamp")
    private SortField sortField = SortField.CREATION_TIMESTAMP;

    @NotNull
    @Schema(example = "DESC")
    private SortOrder sortOrder = SortOrder.DESC;

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public SortField getSortField() {
        return sortField;
    }

    public void setSortField(SortField sortField) {
        this.sortField = sortField;
    }

    public SortOrder getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }
}
