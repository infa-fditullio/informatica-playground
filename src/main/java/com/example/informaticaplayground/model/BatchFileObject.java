package com.example.informaticaplayground.model;

import java.util.ArrayList;
import java.util.List;

public class BatchFileObject {
    private List<BatchFileObjectValue> values = null;

    public List<BatchFileObjectValue> getValues() {
        return values;
    }

    public BatchFileObject addValue(BatchFileObjectValue value) {
        if (this.values == null) {
            this.values = new ArrayList<>();
        }

        this.values.add(value);
        return this;
    }
}
