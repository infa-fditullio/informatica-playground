package com.example.informaticaplayground.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;

import java.time.Instant;

public class ApiConsumerAccessResponse {

    @Schema(example = "01234567-89ab-cdef-0123-4567890abcde")
    private String id;

    @Schema(example = "Acc000001")
    private String custRefId;

    private ApiOrderResponse order;

    private APIUser consumer;

    @Schema(description = "This is the user that fulfills the access")
    private APIUser approver;

    private APIDataCollectionResponse dataCollection;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "UTC")
    private Instant creationTimestamp;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "UTC")
    private Instant updatedTimestamp;

    private APIProvisionedTargetResponse grantedProvisionedTarget;

    private ApiConsumerAccessStatus status;

    public String getId() {
        return id;
    }

    public ApiConsumerAccessResponse withId(String id) {
        this.id = id;
        return this;
    }

    public String getCustRefId() {
        return custRefId;
    }

    public ApiConsumerAccessResponse withCustRefId(String custRefId) {
        this.custRefId = custRefId;
        return this;
    }

    public ApiOrderResponse getOrder() {
        return order;
    }

    public ApiConsumerAccessResponse withOrder(ApiOrderResponse order) {
        this.order = order;
        return this;
    }

    public APIUser getConsumer() {
        return consumer;
    }

    public ApiConsumerAccessResponse withConsumer(APIUser consumer) {
        this.consumer = consumer;
        return this;
    }

    public APIUser getApprover() {
        return approver;
    }

    public ApiConsumerAccessResponse withApprover(APIUser approver) {
        this.approver = approver;
        return this;
    }

    public APIDataCollectionResponse getDataCollection() {
        return dataCollection;
    }

    public ApiConsumerAccessResponse withDataCollection(APIDataCollectionResponse dataCollection) {
        this.dataCollection = dataCollection;
        return this;
    }

    public Instant getCreationTimestamp() {
        return creationTimestamp;
    }

    public ApiConsumerAccessResponse withCreationTimestamp(Instant creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
        return this;
    }

    public Instant getUpdatedTimestamp() {
        return updatedTimestamp;
    }

    public ApiConsumerAccessResponse withUpdatedTimestamp(Instant updatedTimestamp) {
        this.updatedTimestamp = updatedTimestamp;
        return this;
    }

    public APIProvisionedTargetResponse getGrantedProvisionedTarget() {
        return grantedProvisionedTarget;
    }

    public ApiConsumerAccessResponse withGrantedProvisionedTarget(APIProvisionedTargetResponse grantedProvisionedTarget) {
        this.grantedProvisionedTarget = grantedProvisionedTarget;
        return this;
    }

    public ApiConsumerAccessStatus getStatus() {
        return status;
    }

    public ApiConsumerAccessResponse withStatus(ApiConsumerAccessStatus status) {
        this.status = status;
        return this;
    }
}
