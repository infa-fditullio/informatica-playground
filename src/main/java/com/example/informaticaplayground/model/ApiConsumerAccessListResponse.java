package com.example.informaticaplayground.model;

import java.util.List;

public class ApiConsumerAccessListResponse {

    private List<ApiConsumerAccessResponse> objects;
    private Long totalObjects;

    public ApiConsumerAccessListResponse(List<ApiConsumerAccessResponse> objects, Long totalObjects) {
        this.objects = objects;
        this.totalObjects = totalObjects;
    }

    public List<ApiConsumerAccessResponse> getObjects() {
        return objects;
    }

    public void setObjects(List<ApiConsumerAccessResponse> objects) {
        this.objects = objects;
    }

    public Long getTotalObjects() {
        return totalObjects;
    }

    public void setTotalObjects(Long totalObjects) {
        this.totalObjects = totalObjects;
    }
}
