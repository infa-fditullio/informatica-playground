package com.example.informaticaplayground.model;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class APIUser {

    @Schema(example = "01234567-89ab-cdef-0123-4567890abcde")
    private String id;
    @NotNull
    @Pattern(regexp = "^(?!\\s*$).+", message = "{com.infa.products.marketplace.domain.usecases.user.api.APIUser.ref.message}")
    @Size(max = 255)
    private String ref;

    private String name;

    @Schema(example = "name@example.com")
    private String email;

    public String getId() {
        return id;
    }

    public APIUser withId(String id) {
        this.id = id;
        return this;
    }

    public String getRef() {
        return ref;
    }

    public APIUser withRef(String ref) {
        this.ref = ref;
        return this;
    }

    public String getName() {
        return name;
    }

    public APIUser withName(String name) {
        this.name = name;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public APIUser withEmail(String email) {
        this.email = email;
        return this;
    }
}
