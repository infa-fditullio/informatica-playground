package com.example.informaticaplayground.model;

import javax.validation.constraints.NotNull;

public class APIProvisionedTargetResponse {

    private String id;

    private String physicalLocation;

    private String description;

    @NotNull
    private String statusId;

    public String getId() {
        return id;
    }

    public APIProvisionedTargetResponse withId(String id) {
        this.id = id;
        return this;
    }

    public String getPhysicalLocation() {
        return physicalLocation;
    }

    public APIProvisionedTargetResponse withPhysicalLocation(String physicalLocation) {
        this.physicalLocation = physicalLocation;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public APIProvisionedTargetResponse withDescription(String description) {
        this.description = description;
        return this;
    }

    public String getStatusId() {
        return statusId;
    }

    public APIProvisionedTargetResponse withStatusId(String statusId) {
        this.statusId = statusId;
        return this;
    }
}
