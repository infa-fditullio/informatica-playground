package com.example.informaticaplayground.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;

import java.time.Instant;

public class ApiOrderResponse {

    @Schema(example = "01234567-89ab-cdef-0123-4567890abcde")
    private String id;

    @Schema(example = "Ord000001")
    private String custRefId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "UTC")
    private Instant creationTimestamp;

    private String justification;

    public String getId() {
        return id;
    }

    public ApiOrderResponse withId(String id) {
        this.id = id;
        return this;
    }

    public String getCustRefId() {
        return custRefId;
    }

    public ApiOrderResponse withCustRefId(String custRefId) {
        this.custRefId = custRefId;
        return this;
    }

    public Instant getCreationTimestamp() {
        return creationTimestamp;
    }

    public ApiOrderResponse withCreationTimestamp(Instant creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
        return this;
    }

    public String getJustification() {
        return justification;
    }

    public ApiOrderResponse withJustification(String justification) {
        this.justification = justification;
        return this;
    }
}
