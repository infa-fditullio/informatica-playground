package com.example.informaticaplayground.model;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ApiConsumerAccessStatus {
    ACTIVE("ACTIVE"),
    REVOKED("REVOKED"),
    PENDING_REVOKE("PENDING REVOKE");

    private final String value;

    ApiConsumerAccessStatus(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
