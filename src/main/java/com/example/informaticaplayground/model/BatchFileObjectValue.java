package com.example.informaticaplayground.model;

public class BatchFileObjectValue {
    private String id;
    private String text;

    public String getId() {
        return id;
    }

    public BatchFileObjectValue withId(String id) {
        this.id = id;
        return this;
    }

    public String getText() {
        return text;
    }

    public BatchFileObjectValue withText(String text) {
        this.text = text;
        return this;
    }
}
