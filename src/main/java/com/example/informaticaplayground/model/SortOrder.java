package com.example.informaticaplayground.model;

public enum SortOrder {
    ASC,
    DESC
}
