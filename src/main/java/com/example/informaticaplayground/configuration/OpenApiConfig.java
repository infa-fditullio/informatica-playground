package com.example.informaticaplayground.configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityScheme;

@OpenAPIDefinition(info = @Info(title = "Informatica Playground",
        description = "Informatica playground is designed to generate a functional data processor which will allow migrating data from On-Prem Data Marketplace to the Cloud.", version = "v1"))
@SecurityScheme(name = "Token", type = SecuritySchemeType.HTTP, scheme = "bearer", bearerFormat = "JWT")
public class OpenApiConfig {
}
