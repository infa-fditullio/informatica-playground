package com.example.informaticaplayground.controller;

import com.example.informaticaplayground.model.ApiConsumerAccessFilter;
import com.example.informaticaplayground.model.ApiConsumerAccessListResponse;
import com.example.informaticaplayground.service.OnPremService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("dmp")
@Tag(name = "Data Marketplace On Prem API", description = "This APIs are from DMP product")
public class OnPremController {

    @Autowired
    private OnPremService service;

    @Operation(summary = "Fetch the Consumer Accesses for Admin Users, Technical Users and Technical Owners at DataCollection Stakeholder level",
            description = "If there are filters provided in the body, the scope of fetch is confined to consumer accesses of those")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiConsumerAccessListResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Bad Request",
                    content = @Content),
            @ApiResponse(responseCode = "422", description = "Unprocessable Entity",
                    content = @Content)})
    @PostMapping("/consumeraccess/datacollection")
    @SecurityRequirement(name = "Token")
    public ApiConsumerAccessListResponse getFilteredDataCollectionConsumerAccess(@RequestBody ApiConsumerAccessFilter userCredential) {
        return service.fetchDataCollectionConsumerAccess(userCredential);
    }
}
