package com.example.informaticaplayground.controller;

import com.example.informaticaplayground.model.BatchFileContent;
import com.example.informaticaplayground.service.CloudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.IOException;

@Controller
@RequestMapping("cdmp")
public class CloudController {

    @Autowired
    private CloudService service;

    @GetMapping("/file-upload")
    public String fileUploader(Model model) {
        return "cdmp-file-uploader";
    }

    @PostMapping("/file-upload")
    public String fileUploaderProcessor(@NotNull @RequestParam("file") MultipartFile file, Model model) {
        try {
            BatchFileContent batchFileContent  = service.processFile(file.getInputStream());
            model.addAttribute("headers", batchFileContent.getHeaders());
            model.addAttribute("records", batchFileContent.getRecords());
            model.addAttribute("error", false);

        } catch (IllegalArgumentException e) {
            model.addAttribute("error", true);
            model.addAttribute("errorDetails", e.getMessage());
        } catch (IOException e) {
            model.addAttribute("error", true);
            model.addAttribute("errorDetails", "Error while opening the file");
        }
        model.addAttribute("filename", file.getOriginalFilename());
        return "cdmp-file-upload-response";
    }

}
