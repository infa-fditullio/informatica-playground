package com.example.informaticaplayground;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.example.informaticaplayground.configuration","com.example.informaticaplayground.service","com.example.informaticaplayground.controller"})
public class InformaticaPlaygroundApplication {

	public static void main(String[] args) {
		SpringApplication.run(InformaticaPlaygroundApplication.class, args);
	}

}
